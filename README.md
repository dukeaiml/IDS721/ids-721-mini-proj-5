# IDS 721 Mini Proj 5 [![pipeline status](https://gitlab.com/dukeaiml/IDS721/ids-721-mini-proj-5/badges/main/pipeline.svg)](https://gitlab.com/dukeaiml/IDS721/ids-721-mini-proj-5/-/commits/main)

## Overview
* This repository includes the components for **Mini-Project 5 - Serverless Rust Microservice**

## Goal
* Create a Rust AWS Lambda function
* Implement a simple service
* Connect to a databse (DynamoDB)

## My Lambda Function
* My script is written in Rust and uses the Cargo Lambda framework. It contains two functions:
  * `uppercase` - This function takes a string as input and returns the string in uppercase
  * `lowercase` - This function takes a string as input and returns the string in lowercase

## Key Steps
1. Install Rust and Cargo Lambda per the instructions in the [Cargo Lambda documentation](https://www.cargo-lambda.info/guide/installation.html) and [Rust documentation](https://www.rust-lang.org/tools/install)
2. Create a new Rust project using `cargo lambda new <project_name>`
3. Write the code for the Lambda Function in the `src/main.rs` file
4. Test the Lambda Function by first `cargo lambda watch` to build the project and then `cargo lambda invoke <function_name> --event <event_file>.json` to test the function
5. Sign into AWS and go to the IAM console, add a new user, choose "Attch policies directly" and select "IAMFullAccess" and "AWSLambda_FullAccess"
6. Finish creating the user, navigate to the "Security Credentials" tab, and create an access key
7. Store AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, and AWS_REGION in a newly created `.env` file
8. Create a `.gitignore` file and add `.env` to it to prevent the file from being pushed to Gitlab
9. In the terminal, run the following commands to set the environment variables
```
set -a # automatically export all variables
source .env
set +a
```
10. Run `cargo lambda deploy` to deploy the Lambda Function to AWS
11. Go to the AWS Lambda console and test the function by creating a new test event using the follwing event JSON:
```
{
  "command": "<insert command>"
  "data": "<insert data>"
}
```
12. Add a new trigger to the Lambda Function by selecting "API Gateway", creating a new API, REST API, and "Open" security
13. Deploy the API and test the function using the API Gateway URL using the following format:
```
curl -X POST -H "Content-Type: application/json" -d '{"command": "<insert command>", "data": "<insert data>"}' <API Gateway URL>
```
14. Add permission `AWSDynamoDBFullAccess` to the Lambda Function's role
15. Navigate to the DynamoDB console and create a new table with the respective primary key
16. Now you should have a working Lambda Function that connects to a DynamoDB table and can be called using an API Gateway URL
17. This is my API Gateway URL (https://l6b037e7k5.execute-api.us-east-1.amazonaws.com/default)

## DynamoDB Table Screenshot
![DynamoDB](/uploads/771f8f0fa669ce26c0326d54fcc14d26/DynamoDB.png)

## Lambda Function Diagram
![Lambda_Function](/uploads/7fd91a2ab8e1f1de6fb5fa23c319040a/Lambda_Function.png)

## Successful API Call Screenshot
The first call is for the `uppercase` function and the second call is for the `lowercase` function
![API_Call](/uploads/88db4463b387c411dc345e58088b1fe6/API_Call.png)
